/**
 * This is the entry point to the program.
 *
 * @param {number} noOfWashes The number of times the laundry machine can clean a dirty sock
 * @param {number[]} cleanPile The array of clean socks
 * @param {number[]} dirtyPile The array of dirty socks to wash
 */
function getMaxPairs(noOfWashes, cleanPile, dirtyPile) {
  let cleanSocks = 0;
  let pairs = [];

  for (let i = 0; i < cleanPile.length; i++) {
    if (!pairs.includes(cleanPile[i])) {
      pairs.push(cleanPile[i]);
    } else {
      pairs.splice(pairs.indexOf(cleanPile[i]), 1);
      cleanSocks++;
    }
  }

  if (noOfWashes === 0) {
    return cleanSocks;
  }

  for (let i = 0; i < pairs.length; i++) {
    if (dirtyPile.includes(pairs[i]) && noOfWashes > 0) {
      dirtyPile.splice(dirtyPile.indexOf(pairs[i]), 1);
      cleanSocks++;
      noOfWashes--;
    }
  }

  pairs.length = 0;

  for (let i = 0; i < dirtyPile.length; i++) {
    if (
      noOfWashes > 0 &&
      dirtyPile.indexOf(dirtyPile[i]) !== dirtyPile.lastIndexOf(dirtyPile[i])
    ) {
      if (!pairs.includes(dirtyPile[i])) {
        pairs.push(dirtyPile[i]);
      } else {
        pairs.splice(pairs.indexOf(dirtyPile[i]), 1);
        cleanSocks++;
      }
      noOfWashes--;
    }
  }

  return cleanSocks;
}

module.exports = getMaxPairs;
