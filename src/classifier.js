/**
 * This is the entry point to the program
 *
 * @param {array} input Array of student objects
 */

function classifier(input) {
  const dateToAge = d => {
    const date = new Date(d);
    const year = date.getFullYear();

    const currentDate = new Date();
    const currentYear = currentDate.getFullYear();
    return currentYear - year;
  };
  let slices = [],
    l,
    max = 3,
    output = {};

  arr = input.slice().sort((a, b) => dateToAge(a.dob) - dateToAge(b.dob));

  const createNewSlices = obj => {
    slices.push([obj]);
    l = dateToAge(obj.dob);
  };

  const updateSlices = obj => {
    slices[slices.length - 1].push(obj);
  };

  for (let i = 0; i < arr.length; i++) {
    const age = dateToAge(arr[i].dob);

    if (slices.length === 0) {
      createNewSlices(arr[i]);
      continue;
    }

    if (Math.abs(age - l) <= 5) {
      if (slices[slices.length - 1].length < max) {
        updateSlices(arr[i]);
      } else {
        createNewSlices(arr[i]);
      }
    } else {
      createNewSlices(arr[i]);
    }
  }

  output.noOfGroups = slices.length;

  slices.forEach((item, index) => {
    const ages = item.map(({ dob }) => dateToAge(dob));
    const regNos = item.map(({ regNo }) => +regNo).sort((a, b) => a - b);
    const members = item.map(({ name, regNo, dob }) => ({
      name,
      dob,
      regNo,
      age: dateToAge(dob)
    }));

    output["group" + (index + 1)] = {
      members,
      oldest: Math.max(...ages),
      sum: ages.reduce((acc, cur) => acc + cur, 0),
      regNos
    };
  });

  return output;
}

module.exports = classifier;
